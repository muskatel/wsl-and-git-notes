# Notes on WSL and Git

> Personal version of my notes for setting up WSL and Git for development.

[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://muskatel.gitlab.io/wsl-and-git-notes/)
[![pipeline status](https://gitlab.com/muskatel/wsl-and-git-notes/badges/main/pipeline.svg)](https://gitlab.com/muskatel/wsl-and-git-notes/-/commits/main)

## Maintainers

Craig Marais @muskatel

## License

MIT License, see license file.

---

Copyright 2022, Craig Marais (@muskatel)
