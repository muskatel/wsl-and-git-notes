# Setting up Windows Subsystem for Linux

## Learning Objectives
After completion a learner will be able to ...
- **execute** the needed steps to setup WSL2 and associate environment on Windows 10
- **demonstrate** knowledge of basic Linux filesystem commands

Unix terminal interface provides a more reliable, customisable and standardised way for us to perform commands. For this reason it is suggested that if you are using Windows that you should install the _Windows Subsystem for Linux_ (`WSL`). Later on you will see that this integrates fully into various Microsoft products such as VSCode.

> If you are running Linux (such as on the Jetson  you can skip ahead to setting up ZSH).

If you are running Windows you will want to install WSL. The default will install WSL2 with the `Ubuntu` distribution.

## Installing WSL

> WSL may require additional components from the Windows store.
> I will discuss these steps in class:
>
> - Add required components
>
> - Download Ubuntu 20.04 from store


1) Start by launching `powershell` as Administrator.

Press the windows key or start button and type "`powershell`".

![Launching powershell from start menu](../resources/powershell.jpg)

2) Then run the following command:
```bash
wsl --install
```

Once the command completes installation of WSL, you may need to restart. You can then run the following additional command:

```bash
wsl --install -d Ubuntu
```

If you are running an up-to-date version of Windows 10 then WSL2 should automatically install, for manual instructions please refer to the [official documentation](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

3) You will be prompted to setup a default user and password.
For my own computer I chose the username `craig` as you will see through these notes.

### Virtualization Troubles

You will need to ensure that your processor supports the required virtualisation features.


If you receive an error such as this:
```
Please enable the Virtual Machine Platform Windows feature and ensure virtualisation is enabled in the BIOS.
```

You will need to boot into your bios configuration utility and enable the required technology for your processor.

### Terminating

The following can be performed if you want to do a forced restart of the `Ubuntu` subsystem:

Open up `powershell` as Administrator and run the following command:
```bash
wsl -t ubuntu
```

## Launch WSL and Exploring

We can launch a terminal into the Ubuntu operating system by selecting the `Ubuntu` application for the start menu.

from this new window we have access to the contained version of Linux now running on our Windows computer.

### Some basic Linux Commands

These next few sections will give you a crash course in being able to use Linux. Feel free to skip ahead if you are familiar with these.

#### Working Directory

You can display the working directory (where you are), but using the `pwd` command (**p**rint **w**orking **d**irectory`):

```bash
pwd
/mnt/c/Users/Craig/
```

#### Moving Between Folders

To move between folders we use the `cd` (**c**hange **d**irectory) command.

We can give it either and absolute path or a relative path

**Move to the `C:` directory:**

```bash
cd /mnt/c/
```

**Move to `/mnt/c/Users/Craig/` directory for `/mnt/c`:**

```bash
cd Users/Craig
```

>NOTE: The folder path did not start with a `/`

**Move back a directory:**

```bash
cd ..
```

The `..` denotes the parent directory of the working directory.
These can be chained together with `/` as many times as needed.

**Move back two directories:**

```bash
cd ../..
```

The root directory is denoted with a single `/`.

**Basic Directory Structure:**

```
/                          (root directory)
│
├── home/
│    │
│    ├── craig/            (my user directory & files)
│    │
│    └── ... other users   (none for now)
│
├── mnt/
│    │
│    ├── c/
│    │    │
│    │    └── ... many files
│    │  
│    └── ... other drives  (none for now)
│
└── ... other system files
```

Sometimes it is useful to specify that the path you are typing originates from the working directory explicitly, this is done using a single `.` (period).

**A path starting at the working directory:**

```bash
cd ./Documents/Class_Notes
```

The next handy shortcut is the `~` (tilde), this denotes the users home directory (This uses the `$HOME` variable).

**These commands are equivalent:**

```bash
cd /home/Craig
cd ~
```

#### Listing Folder Contents

The `ls` (**l**i**s**t) command will list the contents of the working directory.

```bash
ls
CV.pdf  Photos  Scan  Videos  backup.zip
```

The `-l` (**l**ist) argument will cause `ls` to display the contents in a list form with details:

```bash
ls -l
total 12
-rw-r--r-- 1 craig craig    0 Sep 17 11:50 CV.pdf
drwxr-xr-x 2 craig craig 4096 Sep 17 11:49 Photos
drwxr-xr-x 2 craig craig 4096 Sep 17 11:49 Scan
drwxr-xr-x 2 craig craig 4096 Sep 17 11:49 Videos
-rw-r--r-- 1 craig craig    0 Sep 17 11:50 backup.zip
```  

The `-a` (**a**ll) argument will cause `ls` to also display the `hidden` folders (usually stuff that starts with `.`):

```bash
ls -a
.  ..  CV.pdf  Photos  Scan  Videos  backup.zip
```

Using a combination of both `-l` and `-a` together in the form of `-al` (or `-la`) with do both, that is all files (`hidden` and other) listed with details:

```bash
ls -al
total 20
drwxr-xr-x  5 craig craig 4096 Sep 17 11:50 .
drwxr-xr-x 10 craig craig 4096 Sep 17 11:53 ..
-rw-r--r--  1 craig craig    0 Sep 17 11:50 CV.pdf
drwxr-xr-x  2 craig craig 4096 Sep 17 11:49 Photos
drwxr-xr-x  2 craig craig 4096 Sep 17 11:49 Scan
drwxr-xr-x  2 craig craig 4096 Sep 17 11:49 Videos
-rw-r--r--  1 craig craig    0 Sep 17 11:50 backup.zip
```

#### Autocomplete of Commands and Paths

While typing commands and paths, pressing `Tab` will attempt to complete the command or path for you.
If multiple options exists they will be listed, and `Tab` will cycle between options.

#### Creating Folders

Folders can be created by using the `mkdir` (**m**a**k**e **dir**ectory) command.

```bash
mkdir ImportantFolder
```

This creates a single folder named `ImportantFolder`

##### Spaces are Bad and Linux is Case Sensitive

The use of spaces in folder names is _**HIGHLY**_ discouraged.
Instead use either `_` (underscores), `-` (dashes), or some form of case convention (`camelCase`,`PascalCase`, or `snake_case`).

**Important Folder name examples**
```
importantfolder
Important_Folder
Important-Folder
ImportantFolder
importantFolder
```

> If you absolutely must specify the use of a space, then you do so by using a `\ ` (backslash and a space)
>
> `mkdir Important\ Folder`
>
> **Do not do this.**
>
> Seriously, it makes navigation much more difficult and causes many other issues.

#### Creating Files

To create files we can use the `touch` command, this command is intended to update the access times of a given file. If the file does not exist it will be created blank instead.

```bash
touch new_file.txt
```

This creates a blank file names `new_file.txt`.

#### Editing Files

**GNU nano**

Nano is a straight-forward editor that uses simple control plus a letter commands:

**For example:**

- Exit: `Ctrl-X`
- Save file: `Ctrl-O`

All commands are listed with shortcuts along the bottom of the screen.

**Install nano:**

Nano is often installed on Linux system, but should you need to install it you can simply use the default package manger for your system.

```bash
sudo apt install nano
```

> nano is not installed by default on the Jetson system image, but it is in WSL

**VIM - Vi IMproved:**

VIM is almost always available on Linux system so it helps to get used to using it.

You start by moving you cursor around (command mode).

To start editing a character press `i` to go into insert mode.

Press `Escape` to go back into command mode.

Commands start with a `:` (colon).

**For example:**

- Exit without saving: `:q` (quit)
- Exit with saving: `:wq` (write and quit)

**VSCode**

Later on we will install and configure VSCode for editing files and we will be able to launch it directly from the working directory.

#### Copying Files

To copy files we use the `cp` (**c**o**p**y) command:

```bash
cp source_file destination_file
```

#### Moving Files

To move files we use the `mv` (**m**o**v**e) command:

```bash
mv source_file destination_file
```

#### Deleting Files

Files can be deleted by suing the `rm` (**r**e**m**ove) command:

```bash
rm file_to_delete
```

#### Deleting Folders

Empty folders can be deleted exactly the same as files:

```bash
rm folder_to_delete
```

Non-empty folder require the use of the `-r` (**r**ecursive) argument.

If any files are protected you will be prompted and you will need to press `Y` for each file.
This can be avoided by using the `-f` (**f**orce) argument.

```bash
pwd
/mnt/c/Users/Craig/Downloads

rm -rf ./*
zsh: sure you want to delete all 2 files in /mnt/c/Users/Craig/Downloads/. [yn]?
```

## Alacritty (a terminal emulator)

As a personal preference I do not like the `Windows Powershell` and instead use `Alacritty`.

[Alacritty on GitHub](https://github.com/alacritty/alacritty)

[Alacritty Releases Page](https://github.com/alacritty/alacritty/releases)

### Alacritty Config

My personal configuration is the following `alacritty.yml` file.
Amongst other things, this is where you set your preferred font. The provided file uses the `Mononoki Nerd Font Mono`.

[alacritty.yml](../resources/alacritty.yml)

Copy this to `%appdata%\Alacitty`, that is: `C:\Users\<your username>\AppData\Roaming\Alacritty`.

The `Mononoki Nerd Font Mono` is available here:

[Nerd Fonts](https://www.nerdfonts.com/font-downloads)

From the Mononoki archive I only use `mononoki-Regular Nerd Font Complete Mono.ttf`

Simply download and install the font.

### Alacritty Colours

You can find alternate colour themes for Alacritty [here](https://github.com/eendroroy/alacritty-theme).

# Setting up ZSH

> You may need to install the fonts as described above.

Now that you have configured `Alacritty`, we need to configure the shell.

We start by running Alacritty and typing the following command:

```bash
sudo apt install zsh
```

This will istall `zsh` (pronounced "zish", rhymes with "fish"), and in future `zsh` will launch automatically, but we can start it now manually byt typing

```bash
zsh
```

We will be customizing `zsh` by using a framework called `oh my zsh` ([oh my zsh homepage](https://ohmyz.sh/)).

We can use `curl` or `wget` to download `oh my zsh` for us:

**curl**
```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

**wget**
```bash
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

By default `oh my zsh` includes a great selection of themes and configurations for us to use, examples of these are visible on the github link: [Themes for oh my zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes).

Finally, we could install the `Power Level 10k` theme which is what I use personally.

## Power Level 10k

Start by cloning the theme directly into the `zsh` custom themes folder:

```bash
git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
```

Edit the `~/.zshrc` file to use the `Power Level 10k` theme:

**Using nano:**
```
nano ~/.zshrc
```

**Using vim:**
```
vim ~/.zshrc
```

Find the line the specifies `ZSH_THEME` and change it to the following:

```
ZSH_THEME="powerlevel10k/powerlevel10k"
```

Then save and close the file.

Once you have saved the theme you can restart `zsh` by simply running zsh:

```bash
zsh
```

If `Power Level 10k` is setup correctly it will start the on screen configuration tool.

You can re-run this configuration again by using the following command:

```bash
p10k configure
```

## Customizing zsh

Start by opening the `~/.zshrc` file for editing:
**Using nano:**
```
nano ~/.zshrc
```

**Using vim:**
```
vim ~/.zshrc
```

At the very bottom you can add aliases (shortcuts) as following:

```
# VSCode
# On my older computer I needed to add this,
# but newer install probably wont need it
alias code='/mnt/c/Users/<your username>/AppData/Local/Programs/Microsoft\ VS\ Code/bin/code
```

This allows launching VSCode directly from the shell,

```bash
code
```

opening a folder,

```bash
# Open here
code .

# Open C:\Users (from WSL)
code /mnt/c/Users/
```

or using VSCode to edit files:

```bash
code ~/.zshrc
```

---

Notes compiled by Craig Marais (@muskatel)
